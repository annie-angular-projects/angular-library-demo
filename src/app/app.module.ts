import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {  MyDemoLibModule } from 'my-demo-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MyDemoLibModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
