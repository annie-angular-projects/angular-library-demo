import { NgModule } from '@angular/core';
import { MyDemoLibComponent } from './my-demo-lib.component';
import { CounterDemoComponent } from './counter-demo/counter-demo.component';
import { BasicHighlightDirective } from './basic-highlight.directive';



@NgModule({
  declarations: [MyDemoLibComponent, CounterDemoComponent, BasicHighlightDirective],
  imports: [
  ],
  exports: [MyDemoLibComponent,CounterDemoComponent,BasicHighlightDirective]
})
export class MyDemoLibModule { }
